import { useHttp } from '../hooks/http.hooks'

const useMarvelServices = () => {
  const { loading, request, error, clearError } = useHttp()

  const _apiBase = 'https://gateway.marvel.com:443/v1/public/',
    _apiKey = 'apikey=00f5b4d79f730456631a770f82af6e83',
    _baseOffset = 210

  const getAllCharacters = async (offset = _baseOffset) => {
    const res = await request(
      `${_apiBase}characters?limit=9&offset=${offset}&${_apiKey}`
    )
    return res.data.results.map(_transformCharacter)
  }

  const getCharacter = async (id) => {
    const res = await request(`${_apiBase}characters/${id}?${_apiKey}`)
    return _transformCharacter(res.data.results[0])
  }

  const _transformCharacter = (char) => {
    if (char.description === '') {
      char.description = 'There is no description of the character'
    }

    if (char.description.length >= 213) {
      char.description = char.description.slice(0, 214) + '...'
    }

    return {
      id: char.id,
      name: char.name,
      description: char.description,
      thumbnail: char.thumbnail.path + '.' + char.thumbnail.extension,
      homePage: char.urls[0].url,
      wiki: char.urls[1].url,
      comics: char.comics.items,
    }
  }

  const getAllComics = async (offset = 0) => {
    const res = await request(
      `${_apiBase}comics?limit=8&offset=${offset}&${_apiKey}`
    )
    return await res.data.results.map(_transformComic)
  }

  const getSingleComic = async (id) => {
    const res = await request(`${_apiBase}comics/${id}?${_apiKey}`)
    return _transformComic(res.data.results[0])
  }

  const _transformComic = (comic) => {
    if (!comic.description) {
      comic.description = 'There is no description of the comic'
    }

    if (comic.prices[0].price === 0) {
      comic.prices[0].price = 'not available'
    }
    return {
      id: comic.id,
      title: comic.title,
      description: comic.description,
      pageCount: comic.pageCount,
      price: comic.prices[0].price,
      imageUrl: `${comic.thumbnail.path}.${comic.thumbnail.extension}`,
    }
  }
  return {
    loading,
    error,
    clearError,
    getAllCharacters,
    getCharacter,
    getAllComics,
    getSingleComic,
  }
}

export default useMarvelServices
