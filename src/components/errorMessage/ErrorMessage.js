import img from './error.gif'

const ErrorMessage = (props) => {
  const { sizes } = props
  return (
    <img
      style={{
        display: 'block',
        width: `${sizes}px`,
        height: `${sizes}px`,
        objectFit: 'contain',
        margin: '0 auto',
      }}
      src={img}
      alt="Error loading"
    />
  )
}

export default ErrorMessage
