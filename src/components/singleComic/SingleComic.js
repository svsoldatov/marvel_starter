import { useState, useEffect } from 'react'

import useMarvelServices from '../../services/MarvelServices'
import Spinner from '../spinner/Spinner'
import ErrorMessage from '../errorMessage/ErrorMessage'

import './singleComic.scss'

const SingleComic = (props) => {
  const { loading, error, getSingleComic, clearError } = useMarvelServices()
  const [comic, setComic] = useState(null)

  const onComicLoaded = (comic) => {
    setComic(comic)
  }

  const updateComic = () => {
    const { comicID } = props
    if (!comicID) {
      return
    } else {
      clearError()
      getSingleComic(comicID).then(onComicLoaded)
    }
  }

  useEffect(() => {
    updateComic()
    console.log('change comic')
  }, [props.comicID])

  const errorMessage = error ? <ErrorMessage sizes={250} /> : null,
    spinner = loading ? <Spinner /> : null,
    content = !(loading || error || !comic) ? <View comic={comic} /> : null
  return (
    <>
      {errorMessage} {spinner} {content}
    </>
  )
}

const View = ({ comic }) => {
  const { title, description, pageCount, imageUrl, price } = comic

  return (
    <div className="single-comic">
      <img src={imageUrl} alt={title} className="single-comic__img" />
      <div className="single-comic__info">
        <h2 className="single-comic__name">{title}</h2>
        <p className="single-comic__descr">{description}</p>
        <p className="single-comic__descr">{pageCount} pages</p>
        <p className="single-comic__descr">Language: en-us</p>
        <div className="single-comic__price">{price}$</div>
      </div>
      <a href="#" className="single-comic__back">
        Back to all
      </a>
    </div>
  )
}

export default SingleComic
