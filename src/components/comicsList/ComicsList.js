import { useState, useEffect } from 'react'
import useMarvelServices from '../../services/MarvelServices'

import ErrorMessage from '../errorMessage/ErrorMessage'
import Spinner from '../spinner/Spinner'

import './comicsList.scss'
import uw from '../../resources/img/UW.png'
import xMen from '../../resources/img/x-men.png'

const ComicsList = (props) => {
  const [comicsList, setComicsList] = useState([]),
    [newItemsLoading, setNewItemsLoading] = useState(false),
    [offset, setOffset] = useState(0),
    [comicEnded, setCharEnded] = useState(false)

  const { loading, error, getAllComics } = useMarvelServices()

  useEffect(() => {
    // в начале - показыает первые 8 карточек
    onRequest(offset, true)
  }, [])

  const onRequest = (offset, initial) => {
    initial ? setNewItemsLoading(false) : setNewItemsLoading(true)
    getAllComics(offset).then(onoComicsListLoaded)
  }

  const onoComicsListLoaded = (newComicsList) => {
    let ended = false
    if (newComicsList.length < 8) {
      ended = true
    }
    setComicsList((comicsList) => [...comicsList, ...newComicsList])
    setNewItemsLoading(false)
    setOffset((offset) => offset + 8)
    setCharEnded((comicEnded) => ended)
  }

  function renderCharachters(arrComics) {
    const items = arrComics.map((comic) => {
      const { id } = comic
      return (
        <ListComicItem
          tabIndex="0"
          onComicSelected={props.onComicSelected}
          key={id}
          comic={comic}
        />
      )
    })
    return <ul className="comics__grid">{items}</ul>
  }

  const items = renderCharachters(comicsList)
  const errorMessage = error ? <ErrorMessage sizes={200} /> : null,
    spinner = loading && !newItemsLoading ? <Spinner /> : null
  return (
    <div className="comics__list">
      {spinner} {errorMessage} {items}
      <button
        className="button button__main button__long"
        style={{ background: newItemsLoading ? '#74010f' : '#9F0013' }}
      >
        <div
          style={{
            display: comicEnded ? 'none' : 'block',
          }}
          disabled={newItemsLoading}
          onClick={() => onRequest(offset)}
          className="inner"
        >
          load more
        </div>
      </button>
    </div>
  )
}

const ListComicItem = (props) => {
  const { comic, tabIndex, onComicSelected } = props,
    { id, title, imageUrl, price } = comic
  return (
    <li
      className="comics__item"
      onClick={() => onComicSelected(id)}
      onKeyDown={(e) => {
        if (e.code === 'Enter') {
          onComicSelected(comic)
        }
      }}
      tabIndex={tabIndex}
    >
      <a href="#">
        <img src={imageUrl} alt={title} className="comics__item-img" />
        <div className="comics__item-name">{title}</div>
        <div className="comics__item-price">{price}</div>
      </a>
    </li>
  )
}

export default ComicsList
