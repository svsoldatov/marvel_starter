import { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

import useMarvelServices from '../../services/MarvelServices'
import { ImgChar } from '../randomChar/RandomChar'
import Spinner from '../spinner/Spinner'
import ErrorMessage from '../errorMessage/ErrorMessage'
import Skeleton from '../skeleton/Skeleton'

import './charInfo.scss'

const CharInfo = (props) => {
  const [char, setChar] = useState(null)

  const { loading, error, getCharacter, clearError } = useMarvelServices()

  const onCharLoaded = (char) => {
    setChar(char)
  }

  const updateChar = () => {
    const { charID } = props
    if (!charID) {
      return
    } else {
      clearError()
      getCharacter(charID).then(onCharLoaded)
    }
  }

  useEffect(() => {
    updateChar()
  }, [])

  useEffect(() => {
    updateChar()
    console.log('change char')
  }, [props.charID])

  const skeleton = char || loading || error ? null : <Skeleton />
  const errorMessage = error ? <ErrorMessage sizes={250} /> : null,
    spinner = loading ? <Spinner /> : null,
    content = !(loading || error || !char) ? <View char={char} /> : null

  return (
    <div className="char__info">
      {skeleton}
      {errorMessage}
      {spinner}
      {content}
    </div>
  )
}

const View = ({ char }) => {
  const { name, description, thumbnail, homePage, wiki, comics } = char
  let newComics
  if (comics.length === 0) {
    newComics = 'This character does not appear in any comics'
  } else {
    newComics = comics.map((item, i) => {
      if (i <= 9) {
        return (
          <li key={i} className="char__comics-item">
            {item.name}
          </li>
        )
      }
      return null
    })
  }
  return (
    <>
      <div className="char__basics">
        <ImgChar thumbnail={thumbnail} alt={name} />
        <div>
          <div className="char__info-name">{name}</div>
          <div className="char__btns">
            <a href={homePage} className="button button__main">
              <div className="inner">homepage</div>
            </a>
            <a href={wiki} className="button button__secondary">
              <div className="inner">Wiki</div>
            </a>
          </div>
        </div>
      </div>
      <div className="char__descr">{description}</div>
      <div className="char__comics">Comics:</div>
      <ul className="char__comics-list">{newComics}</ul>
    </>
  )
}

// значение ключа - валидация значения (ожидаемый тип значения)
CharInfo.propTypes = {
  charID: PropTypes.number,
}

export default CharInfo
