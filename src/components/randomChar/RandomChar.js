import { useState, useEffect } from 'react'
import Spinner from '../spinner/Spinner'
import useMarvelServices from '../../services/MarvelServices'
import ErrorMessage from '../errorMessage/ErrorMessage'

import './randomChar.scss'
import mjolnir from '../../resources/img/mjolnir.png'

const RandomChar = () => {
  const [char, setChar] = useState({})

  const { loading, error, getCharacter, clearError } = useMarvelServices()

  useEffect(() => {
    updateChar()
  }, [])

  const onCharLoaded = (char) => {
    setChar(char)
  }

  const updateChar = () => {
    clearError()
    const id = Math.floor(Math.random() * (1011400 - 1011000) + 1011000)
    getCharacter(id).then(onCharLoaded)
  }

  const errorMessage = error ? <ErrorMessage sizes={250} /> : null,
    spinner = loading ? <Spinner /> : null,
    content = !(loading || error) ? <View char={char} /> : null

  return (
    <div className="randomchar">
      {errorMessage}
      {spinner}
      {content}
      <div className="randomchar__static">
        <p className="randomchar__title">
          Random character for today!
          <br />
          Do you want to get to know him better?
        </p>
        <p className="randomchar__title">Or choose another one</p>
        <button onClick={updateChar} className="button button__main">
          <div className="inner">try it</div>
        </button>
        <img src={mjolnir} alt="mjolnir" className="randomchar__decoration" />
      </div>
    </div>
  )
}

const ImgChar = ({ thumbnail, alt }) => {
  const flag =
    thumbnail ===
    'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg'

  return (
    <img
      src={thumbnail}
      style={flag ? { objectFit: 'contain' } : { objectFit: 'cover' }}
      alt={alt}
      className="randomchar__img"
    />
  )
}

const View = ({ char }) => {
  const { name, description, homePage, wiki, thumbnail } = char

  return (
    <div className="randomchar__block">
      <ImgChar thumbnail={thumbnail} alt={name} />
      <div className="randomchar__info">
        <p className="randomchar__name">{name}</p>
        <p className="randomchar__descr">{description}</p>
        <div className="randomchar__btns">
          <a href={homePage} className="button button__main">
            <div className="inner">homepage</div>
          </a>
          <a href={wiki} className="button button__secondary">
            <div className="inner">Wiki</div>
          </a>
        </div>
      </div>
    </div>
  )
}

export { ImgChar }
export default RandomChar
