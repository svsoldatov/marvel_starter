import { useState } from 'react'

import ComicsList from '../comicsList/ComicsList'
import SingleComic from '../singleComic/SingleComic'
import ErrorBoundary from '../errorBoundary/ErrorBoundary'
import AppBanner from '../appBanner/AppBanner'

// <ErrorBoundary>
//   <SingleComic comicID={selectedComic}></SingleComic>
// </ErrorBoundary>

const ComicsPage = () => {
  const [selectedComic, setComic] = useState(null)

  const onComicSelected = (id) => {
    setComic(id)
  }

  return (
    <>
      {' '}
      <AppBanner />
      <ErrorBoundary>
        <ComicsList onComicSelected={onComicSelected} />
      </ErrorBoundary>
    </>
  )
}

export default ComicsPage
