import { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

import useMarvelServices from '../../services/MarvelServices'
import Spinner from '../spinner/Spinner'
import ErrorMessage from '../errorMessage/ErrorMessage'
import { ImgChar } from '../randomChar/RandomChar'

import './charList.scss'

const CharList = (props) => {
  const [charList, setCharList] = useState([]),
    [newItemsLoading, setNewItemsLoading] = useState(false),
    [offset, setOffset] = useState(210),
    [charEnded, setCharEnded] = useState(false)

  const { loading, error, getAllCharacters } = useMarvelServices()

  useEffect(() => {
    // в начале - показыает первые 9 карточек
    console.log('did mount char list')
    onRequest(offset, true)
  }, [])

  const onRequest = (offset, initial) => {
    initial ? setNewItemsLoading(false) : setNewItemsLoading(true)
    getAllCharacters(offset).then(onCharListLoaded)
  }

  const onCharListLoaded = (newCharList) => {
    let ended = false
    if (newCharList.length < 9) {
      ended = true
    }
    setCharList((charList) => [...charList, ...newCharList])
    setNewItemsLoading(false)
    setOffset((offset) => offset + 9)
    setCharEnded((charEnded) => ended)
  }

  function renderCharachters(arrChar) {
    const items = arrChar.map((char, ind) => {
      return (
        <ListCharItem
          tabIndex="0"
          onCharSelected={props.onCharSelected}
          key={ind}
          char={char}
        />
      )
    })
    return <ul className="char__grid">{items}</ul>
  }

  const items = renderCharachters(charList)
  const errorMessage = error ? <ErrorMessage sizes={200} /> : null,
    spinner = loading && !newItemsLoading ? <Spinner /> : null

  return (
    <div className="char__list">
      {spinner} {errorMessage} {items}
      <button
        className="button button__main button__long"
        disabled={newItemsLoading}
        style={{ display: charEnded ? 'none' : 'block' }}
        onClick={() => onRequest(offset, false)}
      >
        <div className="inner">load more</div>
      </button>
    </div>
  )
}

const ListCharItem = (props) => {
  const { char, onCharSelected, tabIndex } = props,
    { name, thumbnail, id } = char
  return (
    <li
      tabIndex={tabIndex}
      aria-labelledby="card_label"
      onClick={() => onCharSelected(id)}
      onKeyDown={(e) => {
        if (e.code === 'Enter') {
          onCharSelected(id)
        }
      }}
      // onDragEnter={() => onCharSelected(id)}
      onFocus={(e) => {
        e.target.classList.add('char__item_selected')
      }}
      onBlur={(e) => {
        e.target.classList.remove('char__item_selected')
      }}
      className="char__item"
    >
      <ImgChar thumbnail={thumbnail} alt={name} />
      <div className="char__name">{name}</div>
    </li>
  )
}

CharList.propTypes = {
  onCharSelected: PropTypes.func.isRequired,
}

export default CharList
